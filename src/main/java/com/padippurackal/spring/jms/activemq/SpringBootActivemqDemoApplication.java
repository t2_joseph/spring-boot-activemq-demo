package com.padippurackal.spring.jms.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootActivemqDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootActivemqDemoApplication.class, args);
	}
}
