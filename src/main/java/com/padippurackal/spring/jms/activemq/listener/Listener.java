package com.padippurackal.spring.jms.activemq.listener;

import com.google.gson.Gson;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.Map;

@Component
public class Listener {

    @JmsListener(destination = "inbound.queue")
    @SendTo("outbound.queue")
    public String receiveMessage(final Message jsonMessage) throws JMSException {
        System.out.println("Received Message: " + jsonMessage );

        String messageData = null;
        if(jsonMessage instanceof TextMessage) {
            TextMessage textMessage = (TextMessage)jsonMessage;
            messageData = textMessage.getText();

            Map<String, String> map = new Gson().fromJson(messageData, Map.class);
            return "Hello " + map.get("name");
        }

        return null;
    }

    @JmsListener(destination = "outbound.queue")
    @SendTo("outbound.topic")
    public String receiveMessageFromOutboundQueue(final Message jsonMessage) throws JMSException {
        System.out.println("Received Message from outbound queue: " + jsonMessage );

        String messageData = null;
        if(jsonMessage instanceof TextMessage) {
            TextMessage textMessage = (TextMessage)jsonMessage;
            messageData = textMessage.getText();

            return messageData;
        }

        return null;
    }
}
